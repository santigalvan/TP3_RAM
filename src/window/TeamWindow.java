package window;


import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import business.Manager;

import java.util.ArrayList;
import java.util.function.Predicate;

import javax.swing.JComboBox;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.ImageIcon;

public class TeamWindow extends JDialog {

	private static final long serialVersionUID = 1L;
	private Info info;
	private ArrayList<JTextField> camposJugadores;
	private ArrayList<JTextField> nivelJugador;
	private JTextField txtAlineacion;

	//vernombres o evaluar la posibilidad de hacer esto JTextField[][] incongruencias;
	private ArrayList<JTextField> incongruencias1;
	private ArrayList<JTextField> incongruencias2;
	
	@SuppressWarnings("rawtypes")
	private JComboBox[] posiciones;
	private String[] posicionesValidas = {"GoalKeeper", "CentralDefender", "RightBack", "LeftBack", "MidFielder", "RightWinger", "LeftWinger", "Striker" };
	
	private JComboBox<String> modoResolucion; //ver nombre
	private String[] resoluciones = {"Fuerza bruta", "Backtraking", "Heuristica Exponencial", "Heuristica Golosa" };
	
	private JLabel fondo;
	private JLabel escudo;
	
	private JLabel bAccept;
	private JLabel bCancel;
	private JLabel bInfo;
	private JLabel bCargar;
	
	private ArrayList<String> formacion;
	private String event;
	private Manager manager;

	public TeamWindow(JFrame frmWorldCupAplication, boolean modal)
	{
		
		super(frmWorldCupAplication, modal);
		setTitle("Ingresar equipo");
		setResizable(false);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds((int)screenSize.getWidth()/6, (int) screenSize.getHeight()/6, 1024, 768);
		getContentPane().setLayout(null);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		
		
		manager = new Manager();
		info = new Info(frmWorldCupAplication, true);
		
		camposJugadores = new ArrayList<JTextField>(22);
		posiciones = new JComboBox[22];
		nivelJugador = new ArrayList<JTextField>(22);
		
		incongruencias1 = new ArrayList<JTextField>(12);
		incongruencias2 = new ArrayList<JTextField>(12);
		//Campos de ingreso de datos
		crearCampoJugador();
		crearJComboBoxes();
		crearCampoNivelJugadores();
		
		//Campos de configuracion
		crearJBoxResoluciones();
		crearCampoAlineacion();
		crearCampoIncongruencias();
		
		//Botones
		botonCargar();
		infoButton();
		acceptButton();
		cancelButton();
		
		//Estetico
		dibujarEscudo();
		fondo();
		
	}

	private void crearCampoIncongruencias()
	{
		int posYagregado = 0;
		int posXagregado = 210;
		for(int i=0; i< 12; i++)
		{
			incongruencias1.add(new JTextField());
			incongruencias1.get(i).setBorder(null);
			incongruencias1.get(i).setFont(new Font("Myriad Hebrew", Font.PLAIN, 17));
			incongruencias1.get(i).setBounds(595, 360+posYagregado, 160, 20);
			getContentPane().add(incongruencias1.get(i));
			incongruencias1.get(i).setColumns(10);
			
			JTextField flc = new JTextField("->");
			flc.setBorder(null);
			flc.setForeground(Color.WHITE);
			flc.setFont(new Font("Myriad Hebrew", Font.PLAIN, 17));
			flc.setBounds(773, 360+posYagregado, 30, 20);
			flc.setOpaque(false);
			getContentPane().add(flc);
			
			incongruencias2.add(new JTextField());
			incongruencias2.get(i).setBorder(null);
			incongruencias2.get(i).setFont(new Font("Myriad Hebrew", Font.PLAIN, 17));
			incongruencias2.get(i).setBounds(595+posXagregado, 360+posYagregado, 160, 20);
			getContentPane().add(incongruencias2.get(i));
			incongruencias2.get(i).setColumns(10);
			
			posYagregado+= 29;
		}
	}
	
	private void crearCampoAlineacion()
	{
		txtAlineacion = new JTextField("4-3-3");
		txtAlineacion.setFont(new Font("Myriad Hebrew", Font.PLAIN, 17));
		txtAlineacion.setBorder(null);
		txtAlineacion.setBounds(823, 236, 150, 20);
		getContentPane().add(txtAlineacion);
		txtAlineacion.setColumns(10);
	}

	private void fondo()
	{
		fondo = new JLabel();
		fondo.setIcon(new ImageIcon(TeamWindow.class.getResource("/image/fondo_Cargar.png")));
		fondo.setBounds(0, -13, 1024, 768);
		getContentPane().add(fondo);
	}

	private void dibujarEscudo()
	{
		escudo = new JLabel(); 
		escudo.setOpaque(false);
		escudo.setIcon(new ImageIcon(TeamWindow.class.getResource("/teamShield/escudo_default.png")));
		escudo.setBounds(742, 29, 95, 147);
		getContentPane().add(escudo);
	}

	private void botonCargar() {
		bCargar = new JLabel("");
		bCargar.setIcon(new ImageIcon(MainForm.class.getResource("/image/bCargarOff.png")));
		bCargar.setBounds(590, 110, 102, 102);
		bCargar.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseEntered(MouseEvent arg0)
			{
				bCargar.setIcon(new ImageIcon(MainForm.class.getResource("/image/bCargarOn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e)
			{
				bCargar.setIcon(new ImageIcon(MainForm.class.getResource("/image/bCargarOff.png")));
			}
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				cargarArchivo();
			}
		});
		getContentPane().add(bCargar);
	}
	
	private void cargarArchivo() 
	{
		JFileChooser fc=new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

		FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.XML", "xml");
		fc.setFileFilter(filtro);
		 
		int seleccion = fc.showOpenDialog(getContentPane());
		if(seleccion == JFileChooser.APPROVE_OPTION)
		{
		    File fichero = fc.getSelectedFile();
		    try{
		    	manager.cargarArchivo(fichero.getAbsolutePath());
//		    	dibujarRedElectrica();
//		    	mapa.repaint();
		    }
		    catch (Exception ex) {
		    	ex.printStackTrace();
		    }
		}
	}
	
	private void cancelButton()
	{
		bCancel = new JLabel("");
		bCancel.setIcon(new ImageIcon(MainForm.class.getResource("/image/bCancelOff.png")));
		bCancel.setBounds(893, 20, 102, 102);
		bCancel.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseEntered(MouseEvent arg0)
			{
				bCancel.setIcon(new ImageIcon(MainForm.class.getResource("/image/bCancelOn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e)
			{
				bCancel.setIcon(new ImageIcon(MainForm.class.getResource("/image/bCancelOff.png")));
			}
			@Override
			public void mouseClicked(MouseEvent arg0) {
				event = "close";
				dispose();
			}
		});
		getContentPane().add(bCancel);
	}

	private void infoButton() {
		bInfo = new JLabel("");
		bInfo.setIcon(new ImageIcon(MainForm.class.getResource("/image/bInfoOff.png")));
		bInfo.setBounds(590, 20, 102, 102);
		bInfo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				bInfo.setIcon(new ImageIcon(MainForm.class.getResource("/image/bInfoOn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				bInfo.setIcon(new ImageIcon(MainForm.class.getResource("/image/bInfoOff.png")));
			}
			@Override
			public void mouseClicked(MouseEvent arg0) {
				info.setVisible(true);
			}
		});
		getContentPane().add(bInfo);
	}
	
	public void acceptButton() 
	{
		bAccept = new JLabel("");
		bAccept.setIcon(new ImageIcon(TeamWindow.class.getResource("/image/bCargarTeam.png")));
		bAccept.setBounds(894, 110, 102, 102);
		bAccept.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
					event = "accept";
					
					manager.loadSquad(cargarPlantel());
					manager.loadInconsistencies(cargarInconsistencies());
					manager.loadAligment(getAlineacion());
					String algoritmo =  getAlgorithm();
					manager.execute(algoritmo);
					formacion = manager.getSolution();
					dispose();					

			}
			
			@Override
			public void mouseEntered(MouseEvent e) 
			{
				bAccept.setIcon(new ImageIcon(TeamWindow.class.getResource("/image/bCargarTeamOn.png")));
				
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				bAccept.setIcon(new ImageIcon(TeamWindow.class.getResource("/image/bCargarTeam.png")));
			}
		});
		getContentPane().add(bAccept);
	}

	private void crearJBoxResoluciones() 
	{
		modoResolucion = new JComboBox<String>(resoluciones);
		modoResolucion.setBorder(null);
		modoResolucion.setBounds(823, 267, 150, 20);
		getContentPane().add(modoResolucion);
	}
	
	private void crearJComboBoxes() 
	{
		int posYagregado = 0;
		for(int i=0; i< 22; i++)
		{
			posiciones[i] = new JComboBox<>(posicionesValidas);
			posiciones[i].setBorder(null);
			posiciones[i].setBounds(350, 65+posYagregado, 173, 20);
			getContentPane().add(posiciones[i]);
			posYagregado+= 29;
		}
	}
	
	private void crearCampoJugador()
	{
		int posYagregado = 0;
		for(int i=0; i< 22; i++)
		{
			camposJugadores.add(new JTextField());
			camposJugadores.get(i).setBorder(null);
			camposJugadores.get(i).setFont(new Font("Myriad Hebrew", Font.PLAIN, 17));
			camposJugadores.get(i).setBounds(50, 65+posYagregado, 173, 20);
			getContentPane().add(camposJugadores.get(i));
			camposJugadores.get(i).setColumns(10);
			posYagregado+= 29;
		}
	}

	private void crearCampoNivelJugadores()
	{
		int posYagregado = 0;
		for(int i=0; i< 22; i++)
		{
			nivelJugador.add(new JTextField());
			nivelJugador.get(i).setBounds(260, 65+posYagregado, 50, 20);
			nivelJugador.get(i).setBorder(null);
			nivelJugador.get(i).setFont(new Font("Myriad Hebrew", Font.PLAIN, 17));
			getContentPane().add(nivelJugador.get(i));
			nivelJugador.get(i).setColumns(10);
			posYagregado+= 29;
			
			JTextField campo = nivelJugador.get(i);
			nivelJugador.get(i).addKeyListener(new KeyAdapter()
			{
				@Override
				public void keyTyped(KeyEvent e) 
				{
					nivelAceptado(e, campo);
				}
			});
		}
	}
	
	private void nivelAceptado(KeyEvent e, JTextField campo) 
	{
		char caracter = e.getKeyChar(); 
		
		if (((caracter < '0') || (caracter > '9')) 
		        && (caracter != KeyEvent.VK_BACK_SPACE)
		        && (caracter != '.' || campo.getText().contains(".")) ) 
		{
		            e.consume();
		            JOptionPane.showMessageDialog(null, "Se admiten numeros y un solo punto (.) ");
		}
		
		if(campo.getText().length() > 0 )
		{
			if ( !campo.getText().contains(".") && (caracter >= '1') && (caracter <= '9'))
			{
				e.consume();
				JOptionPane.showMessageDialog(null, "El nivel del jugador debe estar entre 0 y 10");
			}
		}
    } 

	public String getAlineacion()
	{
		return txtAlineacion.getText();
	}
	
	public ArrayList<String> cargarPlantel() 
	{
		ArrayList<String> ret = new ArrayList<>(22);
		String partial = "";
		
		
		int i = 0;
		while(i < camposJugadores.size() && camposJugadores.get(i).getText().length() > 0  )
		{
			partial = camposJugadores.get(i).getText() + " : " + nivelJugador.get(i).getText() + "(" + posiciones[i].getSelectedItem().toString()+")";
			ret.add(partial);
			partial="";
			
			i++;
		}
		return ret;
	}
	
	public ArrayList<String> cargarInconsistencies() 
	{
		ArrayList<String> ret = new ArrayList<>(12);
		String partial = "";
		int i = 0;
		while(i < incongruencias1.size() && incongruencias1.get(i).getText().length() > 0  )
		{
			partial = incongruencias1.get(i).getText() + ":" + incongruencias2.get(i).getText() ;
			ret.add(partial);
			partial="";
			
			i++;
		}
		return ret;
	}

	public String getAlgorithm() 
	{
		return modoResolucion.getSelectedItem().toString();
	}

	public ArrayList<String> obtenerResultado()
	{
		ArrayList<String> ret = new ArrayList<>();
		for(String s: formacion) 
			ret.add(s);
		
		return ret;
	}

	public String getOperation()
	{
		return event;
	}
	
	/** VERIFICACIONES **/ 
	
	/*private boolean verificacionCompleta() 
	{
		System.out.println("entre a verificacion");
		
		String resultado = "";
		if(camposCompletosLinealmente())
			resultado.concat("CAMPOS COMPLETO, ") ;
		else
			resultado.concat("CAMPOS INCOMPLETOS, ");
			
		if(noHayJugadoresRepetidos())
			resultado.concat("JUGADOR REPETIDO, ");
		else
			resultado.concat("NO HAY JG REPETIDOS, ");
		
		if(plantelCompleto())
			resultado.concat("PLANTEL COMPLETO, ");
		else
			resultado.concat("PLANTEL INCOMPLETO, ");
			
		if(existCandidateIncongruity())
			resultado.concat("EXISTEN TODOS LOS CANDIATOS A INC, ");
		else
			resultado.concat("ALGUNO INEXISTENTE, ");
		
		System.out.println(resultado);
		return camposCompletosLinealmente() && noHayJugadoresRepetidos() && plantelCompleto() && existCandidateIncongruity();
	}
	
	private boolean camposCompletosLinealmente() 
	{
		for (int i = 0; i < posiciones.length; i++) {
			
			if(camposJugadores.get(i).getText().length() > 0 && nivelJugador.get(i).getText().length() == 0)
			{
				JOptionPane.showMessageDialog(null, "Campo de nivel vacio");
				return false;
			}
			
			if(camposJugadores.get(i).getText().length() == 0 && nivelJugador.get(i).getText().length() > 0)
			{
				JOptionPane.showMessageDialog(null, "Campo de jugador vacio");
				return false;
			}
			
		}
		return true;
	}
	
	private boolean noHayJugadoresRepetidos()
	{
		for (int i = 0; i < posiciones.length; i++) 
		{
			if(camposJugadores.get(i).getText().length() > 0 && camposJugadores.get(i+1).getText().length() > 0)
			{
				for (int j = i+1; j < posiciones.length; j++) 
				{
					
					String nombrePlayer1 = camposJugadores.get(i).getText();
					String nivelPlayer1 = nivelJugador.get(i).getText();
					String posicionPlayer1 = posiciones[i].getSelectedItem().toString();
					
					String nombrePlayer2 = camposJugadores.get(j).getText();
					String nivelPlayer2 = nivelJugador.get(j).getText();
					String posicionPlayer2 = posiciones[j].getSelectedItem().toString();
					
					if(nombrePlayer1.equals(nombrePlayer2) && nivelPlayer1.equals(nivelPlayer2) && posicionPlayer1.equals(posicionPlayer2))
					{
						JOptionPane.showMessageDialog(null, "Jugadores repetidos");
						return false;
					}
				}
			}
		}
		return true;
	}
	
	private boolean plantelCompleto()
	{
		int cont = 0;
		for(int i = 0; i < camposJugadores.size(); i++)
		{
			if(camposJugadores.get(i).getText().length()>0) //Sino esta vacio
				cont++;
		}
		if(cont >= getNumJugadores())
			return true;
		
		JOptionPane.showMessageDialog(null, "Debe ingresar al menos "+ getNumJugadores() +" jugadores");
		return false;
	}
	// Usar Stream
	private boolean existCandidateIncongruity()
	{
		return apply(incongruencias1, pl -> camposJugadores.contains(pl)) &&
				apply(incongruencias2, pl -> camposJugadores.contains(pl));
	}
	private int getNumJugadores()
	{
		Integer totalJugadores = 1 + Integer.valueOf(""+txtAlineacion.getText().charAt(0))+ Integer.valueOf(""+txtAlineacion.getText().charAt(2)) + Integer.valueOf(""+txtAlineacion.getText().charAt(4));
		return totalJugadores;
	}
	*/
	
	
	public static boolean apply(ArrayList<JTextField> nombres, Predicate<String> predicate)
	{
		for(JTextField JT: nombres)
			if(predicate.test(JT.getText()))
				return true;
			
		return false;
	}
	
}
