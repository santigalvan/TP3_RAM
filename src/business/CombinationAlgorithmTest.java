package business;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Test;

public class CombinationAlgorithmTest 
{
	
	@Test(expected = IllegalArgumentException.class)
	public void overtakeTest() 
	{
		CombinationAlgorithm<Integer> cm = Utilities.createCombinationArray("[1, 2, 3, 4, 5]");
		cm.taken(7); //k > n
	}
	
	@Test
	public void happyPathTest() 
	{
		CombinationAlgorithm<Integer> cm = Utilities.createCombinationArray("[1, 2, 3, 4, 5]");
		
		ArrayList<Combination<Integer>> expected = Utilities.createCombinations(new String[] {
					"[1, 2, 3]", "[1, 2, 4]", "[1, 2, 5]", "[1, 3, 4]", "[1, 3, 5]",
					"[1, 4, 5]", "[2, 3, 4]", "[2, 3, 5]", "[2, 4, 5]", "[3, 4, 5]"});
		
		ArrayList<Combination<Integer>> obtained = cm.taken(3);//5 tomados de a 3 (5 t 3) = 10
		
		Utilities.equalMatrices(expected, obtained);
	}

	@Test
	public void takenFromZeroTest() 
	{
		CombinationAlgorithm<Integer> cm = Utilities.createCombinationArray("[1, 2, 3, 4, 5]");
		
		ArrayList<Combination<Integer>> expected = new ArrayList<Combination<Integer>>();
		expected.add(new Combination<Integer>()); //n(5) tomados de a 0 da como resultado el conjunto vacío
		
		ArrayList<Combination<Integer>> obtained = cm.taken(0);
		
		Utilities.equalMatrices(expected, obtained);
	}
	
	@Test
	public void takenFromOneTest() 
	{
		CombinationAlgorithm<Integer> cm = Utilities.createCombinationArray("[1, 2, 3, 4, 5, 6, 7, 8]");
	
		ArrayList<Combination<Integer>> expected = Utilities.createCombinations(new String[] {
					"[1]", "[2]", "[3]", "[4]", "[5]", "[6]", "[7]", "[8]"});
		
		ArrayList<Combination<Integer>> obtained = cm.taken(1);
		
		Utilities.equalMatrices(expected, obtained);
	}
	
	@Test
	public void takenFromNTest() 
	{
		CombinationAlgorithm<Integer> instance = Utilities.createCombinationArray("[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]");
		
		ArrayList<Combination<Integer>> expected = Utilities.createCombinations(new String[] {
					"[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"});
		
		ArrayList<Combination<Integer>> obtained = instance.taken(10);
		
		Utilities.equalMatrices(expected, obtained);
	}
	
	@Test
	public void squadCombinationTest()
	{
		Squad instance = Utilities.createSquad(new String[] { 
					"Messi     :  8  (GoalKeeper)",
					"Costi     :  7  (LeftBack)", 
					"Galvan    :  8  (RightBack)",
					"Perez     :  9  (RightWinger)",
					"Fernandez :  9  (RightWinger)",				
					"Dami      :  10 (Striker)"});
		
		CombinationAlgorithm<Player> cm = Utilities.createArrayOf(instance.squad());
		ArrayList<Combination<Player>> obtained= cm.taken(4);
		
		Player messi = instance.getPlayer(0);
		Player costi = instance.getPlayer(1);
		Player galvan = instance.getPlayer(2);
		Player perez = instance.getPlayer(3);
		Player fernandez = instance.getPlayer(4);
		Player dami = instance.getPlayer(5);
		
		ArrayList<Combination<Player>> expected = Utilities.createCombinationsOf(new Player[][] {
					{ messi, costi, galvan, perez },    { messi, costi, galvan, fernandez }, 
					{ messi, costi, galvan, dami },     { messi,costi,perez,fernandez },
					{ messi, costi, perez, dami },      { messi, costi, fernandez, dami },
					{ messi, galvan, perez, fernandez },{ messi, galvan, perez, dami },
					{ messi, galvan, fernandez, dami},  { messi, perez, fernandez, dami},
					{ costi, galvan, perez, fernandez}, { costi, galvan, perez, dami },
					{ costi, galvan, fernandez, dami }, { costi, perez, fernandez, dami },
					{ galvan, perez, fernandez, dami }});
		
		Utilities.equalMatrices(expected, obtained);
	}
	
	@Test
	public void combinationTakenFromTest()	
	{
		ArrayList<ArrayList<Combination<Integer>>> obtained = new ArrayList<ArrayList<Combination<Integer>>>();
		
		ArrayList<Integer> firstSeparate = RegEx.createArray("[1, 2, 3, 4, 5]");
		ArrayList<Integer> secondSeparate = RegEx.createArray("[6, 7, 8]");
		ArrayList<Integer> thirdSeparate = RegEx.createArray("[9, 10]");
		ArrayList<Integer> takenFrom = RegEx.createArray("[3, 2, 2]");
		
	
		ArrayList<ArrayList<Combination<Integer>>> expected = new ArrayList<ArrayList<Combination<Integer>>>();

		ArrayList<Combination<Integer>> e1 = Utilities.createCombinations(new String[] {
					"[1, 2, 3]", "[1, 2, 4]", "[1, 2, 5]", "[1, 3, 4]", "[1, 3, 5]",
					"[1, 4, 5]", "[2, 3, 4]", "[2, 3, 5]", "[2, 4, 5]", "[3, 4, 5]"});
		
		ArrayList<Combination<Integer>> e2 = Utilities.createCombinations(new String[] {
					"[6, 7]", "[6, 8]", "[7, 8]"});
		
		ArrayList<Combination<Integer>> e3 = Utilities.createCombinations(new String[] {
					"[9, 10]"});
		expected.add(e1);
		expected.add(e2);
		expected.add(e3);
		
		ArrayList<ArrayList<Integer>> separateSquad = new ArrayList<ArrayList<Integer>>();
		separateSquad.add(firstSeparate);
		separateSquad.add(secondSeparate);
		separateSquad.add(thirdSeparate);
		
		obtained = CombinationAlgorithm.combinationsTakenFrom(separateSquad, takenFrom);
		
		assertEquals(expected, obtained);
	}
	
	@Test
	public void unionTest()
	{
		Combination<Integer> c1 = new Combination<Integer>(RegEx.createArray("[5, 9 , 10]"));
		Combination<Integer> c2 = new Combination<Integer>(RegEx.createArray("[1, 2]"));
		Combination<Integer> expected = new Combination<Integer>(RegEx.createArray("[5, 9, 10, 1, 2]"));
		
		Combination<Integer> obtained = Combination.union(c1, c2);
		
		assertEquals(expected, obtained);
	}


	@Test
	public void sortCombinationTest() 
	{
		Squad instance = Utilities.createSquad(new String[] { 
					"Messi     :  8  (GoalKeeper)",
					"Costi     :  7  (LeftBack)", 
					"Galvan    :  8  (RightBack)",
					"Perez     :  9  (RightWinger)",
					"Fernandez :  9  (RightWinger)",				
					"Dami      :  10 (Striker)"});
		
		CombinationAlgorithm<Player> cm = Utilities.createArrayOf(instance.squad());
		ArrayList<Combination<Player>> obtained= cm.taken(4);
		
		Player messi = instance.getPlayer(0);
		Player costi = instance.getPlayer(1);
		Player galvan = instance.getPlayer(2);
		Player perez = instance.getPlayer(3);
		Player fernandez = instance.getPlayer(4);
		Player dami = instance.getPlayer(5);
		
		ArrayList<Combination<Player>> expected = Utilities.createCombinationsOf(new Player[][] {
					{ messi, costi, galvan, perez },    { messi, costi, galvan, fernandez }, 
					{ messi, costi, galvan, dami },     { messi,costi,perez,fernandez },
					{ messi, costi, perez, dami },      { messi, costi, fernandez, dami },
					{ messi, galvan, perez, fernandez },{ messi, galvan, perez, dami },
					{ messi, galvan, fernandez, dami},  { messi, perez, fernandez, dami},
					{ costi, galvan, perez, fernandez}, { costi, galvan, perez, dami },
					{ costi, galvan, fernandez, dami }, { costi, perez, fernandez, dami },
					{ galvan, perez, fernandez, dami }});
		
		Utilities.equalMatrices(expected, obtained);
	}
}
