package business;

import org.junit.Test;

public class HeuristicSolverTest {

	@Test
	public void noIncongruityTest() 
	{
		Squad squad = Utilities.createSquad(new String[] { 
				"Guzman     :  8  (GoalKeeper)",
				"Romero     :  9  (GoalKeeper)", 
			    "Marchesin  :  4  (GoalKeeper)",

				"Fazio	    :  4  (CentralDefender)", 
				"Mammana    :  5  (LeftBack)", 
				"Otamendi   :  7  (CentralDefender)",
				"Mercado    :  8  (CentralDefender)", 

				"Acuna      :  6  (RightWinger)",
				"Di Maria   :  7  (LeftWinger)",
				"P.Perez    :  7  (MidFielder)", 
				"Gago       :  8  (MidFielder)",
				"E.Perez    :  9  (RightWinger)",
					
				"Messi      :  10 (Striker)",
				"Benedeto   :  7  (Striker)", 
				"Icardi     :  3 (Striker)", 
				"Dybala     :  9  (Striker)"});
			
		Formation expected = Utilities.createFormation("4-4-2", new String[] {
				"Romero     :  9  (GoalKeeper)", 

				"Fazio	    :  4  (CentralDefender)", 
				"Mammana    :  5  (LeftBack)", 
				"Otamendi   :  7  (CentralDefender)",
				"Mercado    :  8  (CentralDefender)", 

				"Di Maria   :  7  (LeftWinger)",
				"P.Perez    :  7  (MidFielder)", 
				"Gago       :  8  (MidFielder)",
				"E.Perez    :  9  (RightWinger)",
				
				"Dybala     :  9  (Striker)", 
				"Messi      :  10 (Striker)" });
	
		Incongruity inconsistencies = Utilities.incongruityBetween(new String[] { });
		
		HeuristicPolinomy solver = HeuristicPolinomy.complete(squad, inconsistencies, "4-3-3");
		Formation obtained = solver.solve();
		Utilities.samePunctuation(expected, obtained);
	}
	
	@Test
	public void withIncongruityTest() 
	{
		Squad squad = Utilities.createSquad(new String[] { 
				"Guzman     :  8  (GoalKeeper)",
				"Romero     :  9  (GoalKeeper)", 
			    "Marchesin  :  4  (GoalKeeper)",

				"Fazio	    :  4  (CentralDefender)", 
				"Mammana    :  5  (LeftBack)", 
				"Pezzela    :  8  (RightBack)",
				"Mascherano :  10  (LeftBack)", 
				"Otamendi   :  7  (CentralDefender)",
				"Mercado    :  8  (CentralDefender)", 

				"Acuna      :  6  (RightWinger)",
				"Di Maria   :  7  (LeftWinger)",
				"Biglia     :  6  (RightWinger)", 
				"Paredes    :  7  (MidFielder)", 
				"Banega     :  8  (MidFielder)",
				"P.Perez    :  7  (MidFielder)", 
				"Gago       :  8  (MidFielder)",
				"E.Perez    :  9  (RightWinger)",
					
				"Messi      :  10 (Striker)",
				"Benedeto   :  7  (Striker)", 
				"Icardi     :  10 (Striker)", 
				"Dybala     :  9  (Striker)"});
			
		Formation expected = Utilities.createFormation("4-3-3", new String[] {
				"Guzman     :  8  (GoalKeeper)", 

				"Otamendi   :  7  (CentralDefender)",
				"Pezzela    :  8  (RightBack)",
				"Mercado    :  8  (CentralDefender)", 
				"Mascherano :  10 (LeftBack)", 
				
				"Banega     :  8  (MidFielder)",
				"Gago       :  8  (MidFielder)",
				"E.Perez    :  9  (RightWinger)",
				
				"Icardi     :  10 (Striker)", 
				"Dybala     :  9  (Striker)", 
				"Messi      :  10 (Striker)" });
	
		Incongruity inconsistencies = Utilities.incongruityBetween(new String[] {
				"Romero  : 9 (GoalKeeper)  <> Messi : 10 (Striker)",
				"E.Perez : 9 (RightWinger) <> Romero : 10 (GoalKeeper)"});
		
		HeuristicPolinomy solver = HeuristicPolinomy.complete(squad, inconsistencies, "4-3-3");
		Formation obtained = solver.solve();
		Utilities.samePunctuation(expected, obtained);
	}
	
	@Test
	public void fullIncongruityTest() 
	{
		Squad squad = Utilities.createSquad(new String[] { 
				"Guzman     :  8  (GoalKeeper)",
				"Romero     :  9  (GoalKeeper)", 
			    "Marchesin  :  4  (GoalKeeper)",

				"Fazio	    :  4  (CentralDefender)", 
				"Mammana    :  5  (LeftBack)", 
				"Pezzela    :  8  (RightBack)",
				"Mascherano :  10  (LeftBack)", 
				"Otamendi   :  7  (CentralDefender)",
				"Mercado    :  8  (CentralDefender)", 

				"Acuna      :  6  (RightWinger)",
				"Di Maria   :  7  (LeftWinger)",
				"Biglia     :  6  (RightWinger)", 
				"Paredes    :  7  (MidFielder)", 
				"Banega     :  8  (MidFielder)",
				"P.Perez    :  7  (MidFielder)", 
				"Gago       :  8  (MidFielder)",
				"E.Perez    :  9  (RightWinger)",
					
				"Messi      :  10 (Striker)",
				"Benedeto   :  7  (Striker)", 
				"Icardi     :  10 (Striker)", 
				"Dybala     :  9  (Striker)"});
			
		Formation expected = Utilities.createFormation("4-3-3", new String[] { });
	
		Incongruity inconsistencies = Utilities.fullIncongruity(squad);
		
		HeuristicPolinomy solver = HeuristicPolinomy.complete(squad, inconsistencies, "4-3-3");
		Formation obtained = solver.solve();
		
		Utilities.samePunctuation(expected, obtained);
	}

	@Test
	public void noTeamTest() 
	{
		Squad squad = Utilities.createSquad(new String[] { });
			
		Formation expected = Utilities.createFormation("4-3-3", new String[] { });
	
		HeuristicPolinomy solver = HeuristicPolinomy.complete(squad, new Incongruity(), "4-3-3");
		Formation obtained = solver.solve();
		
		Utilities.samePunctuation(expected, obtained);
	}
	
	@Test
	public void teamIncompleteTest() 
	{
		Squad squad = Utilities.createSquad(new String[] { 
				"Guzman     :  8  (GoalKeeper)",
				"Romero     :  9  (GoalKeeper)", 

				"Fazio	    :  4  (CentralDefender)", 
				"Otamendi   :  7  (CentralDefender)",
				"Mercado    :  8  (CentralDefender)", 

				"Gago       :  8  (MidFielder)",
				"E.Perez    :  9  (RightWinger)",
					
				"Dybala     :  9  (Striker)"});
			
		Formation expected = Utilities.createFormation("4-3-3", new String[] { });
	
		HeuristicPolinomy solver = HeuristicPolinomy.complete(squad, new Incongruity(), "4-3-3");
		Formation obtained = solver.solve();
		
		Utilities.samePunctuation(expected, obtained);
	}
	
}
