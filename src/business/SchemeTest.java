package business;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import business.Player.Position;

public class SchemeTest {
	
	Scheme scheme;
	
	@Before
	public void initScheme()
	{
		scheme = new Scheme("4-4-2");
	}
	
	@Test
	public void totalLoadSchemeTest() 
	{	
		assertEquals(new Integer(1), scheme.totalNumberOf("GoalKeeper"));
		assertEquals(new Integer(4), scheme.totalNumberOf("Defense"));
		assertEquals(new Integer(4), scheme.totalNumberOf("MidFielder"));
		assertEquals(new Integer(2), scheme.totalNumberOf("Striker"));
	}
	
	@Test
	public void partialLoadSchemeTest() 
	{	
		Player romero = new Player("Romero", 8, Position.GoalKeeper);
		Player messi = new Player("Messi", 10, Position.Striker);
		
		scheme.sumPosition(messi);
		scheme.sumPosition(romero);
		
		assertEquals(new Integer(1), scheme.partialNumberOf("GoalKeeper"));
		assertEquals(new Integer(1), scheme.partialNumberOf("Striker"));
		
		scheme.delPosition(messi);
		assertEquals(new Integer(0), scheme.partialNumberOf("Striker"));
		
	}
	
	

}
