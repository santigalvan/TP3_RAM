package business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

public class Utilities 
{
	
		static boolean isBetter(Combination<Player> candidate, Formation incumbent) 
		{
			return candidate.stream().mapToDouble(e -> e.getLevel()).sum() > incumbent.appraiseTeam();
		}
		
		/**Metodos de test para FormationTest*/
		public static void samePunctuation(Formation expected, Formation obtained)
		{
			assertEquals(expected.getPlayers().size(), obtained.getPlayers().size());
			assertEquals((int) expected.appraiseTeam(), (int) obtained.appraiseTeam());
		}

		/**Metodos de test para SquadTest*/
		public static void sameSquad(Squad expected, Squad obtained)
		{
			assertEquals(expected.numberOfplayers(), obtained.numberOfplayers());
			
			for(Player player: expected.squad()) 
				assertTrue(obtained.contain(player));
		}

		/**Metodos de test para IncongruityTest*/
		public static Incongruity fullIncongruity(Squad instance) 
		{
			Incongruity ret = new Incongruity();
			for(int i=0; i<instance.squad().size(); i++) 
			{
				for(int j=i+1; j<instance.squad().size(); j++)
				{
					if(!(instance.getPlayer(i).equals(instance.getPlayer(j))) )
						ret.addIncongruity(instance.getPlayer(i), instance.getPlayer(j));
				}
			}
			return ret;
		}
		
		public static void checkIncongruity(Incongruity inc, Player romero, Player messi) {
			assertTrue(inc.containsKey(messi));
			assertTrue(inc.containsKey(romero));
			
			assertEquals(1, inc.listOfIncongruity(messi).size());
			assertEquals(1, inc.listOfIncongruity(romero).size());
		}
		
		public static void incongruityInTeam(Incongruity inconsistencies, ArrayList<Player> players) 
		{
			if(players.size() != 0)
				assertTrue(inconsistencies.inTeam(players));
			
			for(Player p: players)
				assertTrue(inconsistencies.containsKey(p));
		}
		
		public static Incongruity incongruityBetween(String[] source) 
		{
			Incongruity ret = new Incongruity();
			
			for(String s: source) {
				Player[] players = RegEx.incongruity(s);
				ret.addIncongruity(players[0], players[1]);
			}
			return ret;
		}

		/**Metodos de test para CombinationTest*/
		public static <T> void equalMatrices(ArrayList<Combination<T>> expected,
				ArrayList<Combination<T>> obtained) 
		{
			assertEquals(expected.size(), obtained.size());
			
			int k = expected.get(0).size(); //Tamaño de una instancia general tomada de a K
			for(int i = 0; i < expected.size(); i++)
			{
				assertEquals(k, obtained.get(i).size());
				assertTrue(obtained.contains(expected.get(i)));
			}
		}
		
		public static <T> void arreglosIguales(ArrayList<T> expected,
				ArrayList<T> obtained) 
		{
			for(int i = 0; i < expected.size() ; i++){
			}
		}
		
		public static ArrayList<Combination<Integer>> createCombinations(String[] source) {
			ArrayList<Combination<Integer>> ret = new ArrayList<Combination<Integer>>();
			
			for(String s: source) {
				Combination<Integer> c = new Combination<Integer>(RegEx.createArray(s));
				ret.add(c);
			}
			return ret;
		}

		public static <T> CombinationAlgorithm<T> createArrayOf(ArrayList<T> arrayObject) 
		{	
			ArrayList<T> mid = new ArrayList<T>();
			for(T  object: arrayObject)
				mid.add(object);
				
			CombinationAlgorithm<T> ret = new CombinationAlgorithm<T>(mid);	
			return ret;
		}
		
		public static <T> ArrayList<Combination<T>> createCombinationsOf(T[][] objectMatrix) 
		{
			ArrayList<Combination<T>> ret = new ArrayList<Combination<T>>();

			for(int i=0; i<objectMatrix.length; i++) {
				Combination<T> mid = new Combination<T>();
				for(int j=0; j<objectMatrix[i].length; j++) {
					mid.add(objectMatrix[i][j]);
				}
				ret.add(mid);
			}
			return ret;
		}
		
		public static CombinationAlgorithm<Integer> createCombinationArray(String source) {
			ArrayList<Integer> array = RegEx.createArray(source);
			CombinationAlgorithm<Integer> ret = new CombinationAlgorithm<Integer>(array);
			
			return ret;
		}
		
		public static Formation createFormation(String alignment, String[] source) 
		{
			Formation ret = new Formation(alignment);
			
			for(String s: source) 
				ret.add(RegEx.player(s));
			
			return ret;
		}
		
		public static Squad createSquad(String[] source)
		{
			Squad ret = new Squad();
			
			for(String s: source) 
				ret.addPlayer(RegEx.player(s));
			
			return ret;
		}
}

