package business;

import java.util.Objects;

public class Player
{
	private String name;
	private double level;
	
	public enum Position { GoalKeeper, CentralDefender, RightBack, LeftBack, MidFielder, RightWinger, LeftWinger, Striker };
	private Position position;
	
	public Player(String name, double level, Position position)
	{
		if(verify(name, level, position))
		{
			this.name = name;
			this.level = level;
			this.position = position;
		}
		else
			throw new IllegalArgumentException();
	}

	private boolean verify(String name, double level, Position position)
	{
		if(name == null || name.length() == 0)	
			return false;
		
		if(level<0 || level>10)
			return false;
		
		if(position == null)
			return false;
		
		return true;
		
	}

	public String getName() 
	{
		return name;
	}
	
	public double getLevel() 
	{
		return level;
	}
	
	public Position getPosition()
	{
		return position;
	}
	
	public String whatPositionPlays() 
	{
		if(isGoalKeeper())
			return "GoalKeeper";
		
		if(isDefender())
			return "Defense";
	
		if(isMidFielder())
			return "MidFielder";
		
		if(isStriker())
			return "Striker";
		
		return null;
	}
	
	boolean isGoalKeeper() 
	{
		return position.equals(Position.GoalKeeper);
	}
	
	boolean isDefender() 
	{
		return position.equals(Position.CentralDefender) || position.equals(Position.RightBack) || position.equals(Position.LeftBack);
	}
	
	boolean isMidFielder() 
	{
		return position.equals(Position.MidFielder) || position.equals(Position.LeftWinger) || position.equals(Position.RightWinger);
	}
	
	boolean isStriker() 
	{
		return position.equals(Position.Striker);
	}
	
	@Override
	public Player clone(){
		return new Player(name, level, position);
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if(obj == null || obj.getClass() != this.getClass()) return false;
		if(this == obj) return true;
		Player other = (Player) obj;
		if(this.getName().equals(other.getName()) && this.getLevel() == other.getLevel() && this.getPosition().equals(other.getPosition())) return true;
		return false;
	}
	
	@Override 
	public int hashCode()
	{
		return Objects.hash(name, level, position);
	}
	
	@Override
	public String toString() 
	{
		return name +": "+ Double.valueOf(level)+" ("+position+")";
	}

}