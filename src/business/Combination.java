package business;

import java.util.ArrayList;
import java.util.stream.Stream;

public class Combination<T> 
{
	private ArrayList<T> combination;
	
	public Combination(ArrayList<T> combination)
	{
		this.combination = combination;
	}
	
	public Combination()
	{
		this.combination = new ArrayList<T>();
	}
	
	static <T> Combination<T> union(Combination<T> c1, Combination<T> c2)
	{
		Combination<T> union = new Combination<T>();
		appendTo(union, c1);
		appendTo(union, c2);
		
		return union;
	}
	
	private static <T> void appendTo(Combination<T> union, Combination<T> c1) {
		for(T e1: c1.combination)
			union.add(e1);
	}
	
	public void setCombination(ArrayList<T> combination)
	{
		this.combination = combination;
	}
	
	public ArrayList<T> get()
	{
		return this.combination;
	}
	
	public T get(int i) 
	{
		return combination.get(i);
	}
	
	public boolean add(T element)
	{
		return this.combination.add(element);
	}
	
	public boolean remove(T element)
	{
		return this.combination.remove(element);
	}
	
	public int size()
	{
		return this.combination.size();
	}
	
	public boolean contains(Object obj)
	{
		return this.combination.contains(obj);
	}
	
	public Stream<T> stream() 
	{
		return this.combination.stream();
	}
	
	@Override
	public Combination<T> clone()
	{
		ArrayList<T> clone = new ArrayList<T>();
		for(T element : this.combination)
			clone.add(element);
		
		Combination<T> ret = new Combination<T>(clone);
		return ret;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj==null) return false;
		if(!(obj.getClass().equals(this.getClass()))) return false;
		@SuppressWarnings("unchecked")
		Combination<T> otra = (Combination<T>) obj;
		return this.combination.equals(otra.combination);
	}
	
	@Override
	public int hashCode()
	{
		return combination.hashCode();
	}

	@Override
	public String toString() 
	{
		return this.combination.toString();
	}
}
