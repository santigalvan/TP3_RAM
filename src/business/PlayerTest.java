package business;


import static org.junit.Assert.*;

import org.junit.Test;
import business.Player.Position;

public class PlayerTest {

	@Test(expected = IllegalArgumentException.class)
	public void nullParameterTest() 
	{
		@SuppressWarnings("unused")
		Player player = new Player(null, 0.0, null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void overPoweredPlayer() 
	{
		@SuppressWarnings("unused")
		Player player = new Player("P1", 999999, Position.Striker);
	}
	
	@Test
	public void globalPositionTest() 
	{
		Player P1 = new Player("P1", 7.6, Position.GoalKeeper);
		Player P2 = new Player("P2", 6, Position.CentralDefender);
		Player P3 = new Player("P3", 8.0, Position.MidFielder);
		Player P4 = new Player("P4", 9, Position.Striker);
		
		assertEquals("GoalKeeper", P1.whatPositionPlays());
		assertEquals("Defense", P2.whatPositionPlays());
		assertEquals("MidFielder", P3.whatPositionPlays());
		assertEquals("Striker", P4.whatPositionPlays());
	}

}
