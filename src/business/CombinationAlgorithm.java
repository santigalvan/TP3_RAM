package business;

import java.util.ArrayList;
import java.util.List;

public class CombinationAlgorithm <T> 
{
	private ArrayList<T> instance;
	private Combination<T> currentCombination;
	private ArrayList<Combination<T>> combinations;
	
	public CombinationAlgorithm(ArrayList<T> elements)
	{
		this.instance = elements;
		this.currentCombination = new Combination<T>();
	}
	
	public ArrayList<Combination<T>> taken(int k)
	{
		if(k > instance.size())
			throw new IllegalArgumentException();
		
		combinations = new ArrayList<Combination<T>>();
		generateFrom(0, k);
		
		return combinations;
	}
	
	private void generateFrom(int i, int k)
	{
		if(k == 0){
			combinations.add(currentCombination.clone());
			return;
		}

		if(i == instance.size())
			return;
		
		else 
		{
			currentCombination.add(instance.get(i));
			generateFrom(i+1, k-1);
			currentCombination.remove(instance.get(i));
			generateFrom(i+1, k);
		}
	}

	//Take |separateSets[i]| from |numbers[i]| 
	public static <T> ArrayList<ArrayList<Combination<T>>> combinationsTakenFrom(ArrayList<ArrayList<T>> separateSets,
			List<Integer> numbers) 
	{
		if( separateSets.size()!= numbers.size() )
			throw new IllegalArgumentException();
		
		ArrayList<ArrayList<Combination<T>>> ret = new ArrayList<ArrayList<Combination<T>>>();
		ArrayList<Combination<T>> currentCombinations = new ArrayList<Combination<T>>();
		
		for(int i = 0; i < numbers.size(); i ++)
		{
			CombinationAlgorithm<T> combinationAlgorithm = new CombinationAlgorithm<T>(separateSets.get(i));
			currentCombinations =  combinationAlgorithm.taken(numbers.get(i));
			ret.add(currentCombinations);
		}
	
		return ret;
	}
}