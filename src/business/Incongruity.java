package business;

import java.util.ArrayList;
import java.util.HashMap;

public class Incongruity 
{
	private HashMap<Player, ArrayList<Player>> inconsistencies;
	
	public Incongruity()
	{
		this.inconsistencies = new HashMap<>();
	}
	
	public void addIncongruity(Player player1, Player player2)
	{
		if(player1 == null || player2 == null)
			throw new IllegalArgumentException();

		setIncongruityRelation(player1, player2);
		setIncongruityRelation(player2, player1);
	}
	
	private void setIncongruityRelation(Player player1, Player player2) 
	{
		if(inconsistencies.containsKey(player2))
			inconsistencies.get(player2).add(player1);
		else
		{
			ArrayList<Player> playerIncongruity = new ArrayList<>();
			playerIncongruity.add(player1);
			inconsistencies.put(player2, playerIncongruity);
		}
	}

	public boolean inTeam(ArrayList<Player> players)
	{
		for(Player p : players)
			if(existIncongruity(p, players))
				return true;
		
		return false;
	}
	
	public boolean existIncongruity(Player candidate, ArrayList<Player> players)
	{
		if(!containsKey(candidate))
			return false;
		
		for(Player p: listOfIncongruity(candidate)) 
			if(players.contains(p))
				return true;
		
		return false;
	}

	public boolean containsKey(Player candidate) 
	{
		return inconsistencies.containsKey(candidate);
	}
	
	public ArrayList<Player> listOfIncongruity(Player candidate) 
	{
		return inconsistencies.get(candidate);
	}
	
	@Override
	public String toString() 
	{
		String ret = "";
		for(Player p: inconsistencies.keySet())
			ret += p.getName()+": "+ inconsistencies.get(p)+", ";
		return ret;
	}


}
