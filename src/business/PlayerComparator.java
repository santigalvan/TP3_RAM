package business;

import java.util.Comparator;

public class PlayerComparator 
{
	/**Comparadores*/
	//Tendrian que estar en la clase combinacion o player
	public static Comparator<Player> byLevel()
	{	
		return  ((p1, p2) ->  Double.compare(p2.getLevel(),  p1.getLevel()));	
	}
	
	public static Comparator<Combination<Player>> combinationLevel() 
	{
		return  ((c1, c2) ->  Double.compare(getLevel(c2), getLevel(c1)));
	}
	
	private static double getLevel(Combination<Player> combination)
	{
		double ret = 0;
		for (int i = 0; i < combination.size(); i++) 
			ret += combination.get(i).getLevel();
		
		return ret;
	}
}
