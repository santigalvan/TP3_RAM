package business;

import java.util.ArrayList;

public class Formation 
{
	private ArrayList<Player> players;
	private Scheme scheme;
	
	public Formation(String scheme) 
	{
		this.players = new ArrayList<Player>();
		this.scheme = new Scheme(scheme);
	}
	
	public Formation(Scheme scheme)
	{
		this.players = new ArrayList<Player>();
		this.scheme = scheme;
	}
	
	public Formation(ArrayList<Player> players, String scheme) 
	{
		this.players = players;
		this.scheme = new Scheme(scheme);
	}

	public double appraiseTeam() 
	{
		return players.stream().mapToDouble(p -> p.getLevel()).sum();
	}
	
	public boolean isBetter(Formation other) 
	{
		return this.appraiseTeam() > other.appraiseTeam(); 
	}
	
	public boolean allow(Player player)
	{
		String playerAbsolutPosition = player.whatPositionPlays();
		return (scheme.totalNumberOf(playerAbsolutPosition) - scheme.partialNumberOf(playerAbsolutPosition) ) >=1;
	}
	
	public boolean respectScheme() 
	{
		return  scheme.partialNumberOf("GoalKeeper") == scheme.totalNumberOf("GoalKeeper")&&  
				scheme.partialNumberOf("Defense")    == scheme.totalNumberOf("Defense")   &&
				scheme.partialNumberOf("MidFielder") == scheme.totalNumberOf("MidFielder")&& 
				scheme.partialNumberOf("Striker")    == scheme.totalNumberOf("Striker");
	}
	
	public boolean add(Player p)
	{
		scheme.sumPosition(p);
		return players.add(p);
	}
	
	public boolean delete(Player p) 
	{
		scheme.delPosition(p);
		return players.remove(p);
	}
	
	public int numberOfPlayers() 
	{
		return players.size();
	}
	
	public Player getPlayer(int i) 
	{
		return players.get(i);
	}
	
	public ArrayList<Player> getPlayers()
	{
		return players;
	}
	
	public String getScheme() 
	{
		return scheme.toString();
	}
	
	@Override
	public Formation clone()
	{
		Formation ret = new Formation(scheme.toString());
		for(Player player: players)
			ret.add(player);
		
		return ret;
	}

	@Override 
	public String toString() 
	{
		String ret = "[";
		for(int i=0; i<players.size(); i++) 
		{
			if(i == players.size()-1)
				ret += players.get(i).toString();
			else
				ret += players.get(i).toString()+", ";
		}
		return ret+ "]";
	}

}
