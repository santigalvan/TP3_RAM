package business;

import org.junit.Test;

import business.Player.Position;

public class IncongruityTest {
	
	@Test(expected = IllegalArgumentException.class)
	public void nullIncongruityTest()
	{
		Player P1 = null;
		Player P2 = new Player("P2", 10, Position.Striker);
		
		Incongruity inconsistencies = new Incongruity();
		inconsistencies.addIncongruity(P1, P2);
		
		Utilities.checkIncongruity(inconsistencies, P1, P2);
	}
	
	@Test
	public void addIncongruityTest()
	{
		Player Romero = new Player("Romero", 10, Position.GoalKeeper);
		Player Messi = new Player("Messi", 10, Position.Striker);
		
		Incongruity inconsistencies = Utilities.incongruityBetween(new String[] {
				"Romero : 10 (GoalKeeper) <> Messi : 10 (Striker)"});
		
		Utilities.checkIncongruity(inconsistencies, Romero, Messi);
	}
	
	@Test
	public void incongruityInTeamTest()
	{
		Formation instance = Utilities.createFormation("0-0-0", new String[] {
				"Romero     :  10  (GoalKeeper)", 
				"Otamendi   :  7   (CentralDefender)",
				"Banega     :  8   (MidFielder)",
				"Gago       :  8   (MidFielder)",
				"Icardi     :  10  (Striker)", 
				"Benedeto   :  7   (Striker)"});
		
		Incongruity inconsistencies = Utilities.incongruityBetween(new String[] {
				"Romero : 10 (GoalKeeper) <> Otamendi : 7 (CentralDefender)",
				"Icardi : 10 (Striker)    <> Banega   : 8 (MidFielder)",
				"Gago   : 8 (MidFielder)  <> Benedeto : 7 (Striker)"});
		
		Utilities.incongruityInTeam(inconsistencies, instance.getPlayers());
	}
	
	@Test
	public void incongruityWithEmptyTeamTest()
	{
		Formation instance = Utilities.createFormation("0-0-0", new String[] { });
		
		Incongruity inconsistencies = Utilities.incongruityBetween(new String[] {
				"Romero : 10 (GoalKeeper) <> Otamendi : 7 (CentralDefender)",
				"Icardi : 10 (Striker)    <> Banega   : 8 (MidFielder)",
				"Gago   : 8 (MidFielder)  <> Benedeto : 7 (Striker)"});
		
		Utilities.incongruityInTeam(inconsistencies, instance.getPlayers());
	}
}
