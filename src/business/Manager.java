package business;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import business.Player.Position;

public class Manager 
{
	private Logic logic;
	
	public Manager() 
	{
		logic = new Logic();
	}
	
	public void loadAligment(String aligment)
	{
		logic.setScheme(aligment);
	}
	
	public void loadSquad(ArrayList<String> squad) 
	{
		for(String player: squad) 
		{
			ArrayList<String> pl = parseo(player);
			//La interfaz garantiz aque no haya campos repetidos
			System.out.println(pl.get(2));
			logic.loadPlayer(new Player(pl.get(0), Double.valueOf(pl.get(1)), Position.valueOf(pl.get(2))));
		}
	}
	
	//envia datos a la logica de las incongruencias
	public void loadInconsistencies(ArrayList<String> inconsistencies) 
	{
		for(String player: inconsistencies) 
		{
			ArrayList<String> players = parseo(player);
			Player playerOne = logic.getSquad().getPlayerByName(players.get(0));
			Player playerTwo = logic.getSquad().getPlayerByName(players.get(1));
			logic.addIncongruity(playerOne, playerTwo);
		}
		
	}
	
	//tiene que ejecutar el algoritmo o heuristica
	public void execute(String algoritmo) 
	{
		logic.execute(algoritmo);
	}
	
	//le pasamos a mainform
	
	/*public double punctuation()
	{
		return logic.punctuationFormation();
	}
	
	public ArrayList<String> readPlayers()
	{
		 return logic.formationInArray();
	}
	*/
	
	
	private ArrayList<String> parseo(String player) {
		Pattern pattern = Pattern.compile("\\-?\\w(\\.?(\\s?\\w+))*");//Nombres con puntos intermedios y espacios & pesos
		ArrayList<String> ret = new ArrayList<>();
		Matcher matcherPlayer = pattern.matcher(player);
		
		while(matcherPlayer.find()) {
			ret.add(matcherPlayer.group());
		}
		return ret;
	}

	public ArrayList<String> getSolution() 
	{
		return logic.solutionInArray();
	}

	public void cargarArchivo(String absolutePath) 
	{
		// TODO Auto-generated method stub
		
	}
}
