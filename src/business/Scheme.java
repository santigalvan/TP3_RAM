package business;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//Clase creada para tener un control de la formacion actual, 
//es decir que se respete el sistema de juego planteado por el usuario

public class Scheme 
{
	//Representa la cantidad de jugadores player [0] y
	// cuanto es el tope  player[1]
	
	//Por ejemplo: 4-3-3 se representaria de la siguiente manera
	//Posicion(defender, midFielder, striker) { cantidad de jugadores que juegan en esta posicion, jugadores en esta posicion actualmente }

	private int[] goalKeeper;
	private int[] defenders;
	private int[] midFielders;
	private int[] strikers; 
	
	//Ingresa algo como: 4-3-3. EL 1 aca es obvio
	public Scheme(String scheme) 
	{
		schemeRegEx(scheme);
	}
	
	public void schemeRegEx(String source) {
		
		Pattern pattern = Pattern.compile("\\w(\\.?(\\s?\\w+))*");//Nombres con puntos intermedios y espacios & pesos
		
		ArrayList<String> mid = new ArrayList<>();
		
		Matcher matcherSchemePosition = pattern.matcher(source);
		
		while(matcherSchemePosition.find()) {
			mid.add(matcherSchemePosition.group());
		}
		
		goalKeeper = new int[] { 1, 0 };
		defenders = new int[] { Integer.valueOf(mid.get(0)), 0 };
		midFielders = new int[] { Integer.valueOf(mid.get(1)), 0 };
		strikers = new int[] { Integer.valueOf(mid.get(2)), 0 };
	}
	
	//DEvuelvo la string que me pasaron al principio pero como set: ejemplo {1,4,3,3}
	//El 1 aca no es obvio.
	public List<Integer> getScheme()
	{
		ArrayList<Integer> ret = new ArrayList<Integer>();
		ret.add(goalKeeper[0]);
		ret.add(defenders[0]);
		ret.add(midFielders[0]);
		ret.add(strikers[0]);
		
		return ret;
	}
	
	public void sumPosition(Player p) 
	{
		if(p.isGoalKeeper())
			goalKeeper[1]++;
		
		if(p.isDefender())
			defenders[1]++;
		
		if(p.isMidFielder())
			midFielders[1]++;
		
		if(p.isStriker())
			strikers[1]++;
	}

	public void delPosition(Player p) 
	{
		if(p.isGoalKeeper())
			goalKeeper[1]--;
		
		if(p.isDefender())
			defenders[1]--;
		
		if(p.isMidFielder())
			midFielders[1]--;
		
		if(p.isStriker())
			strikers[1]--;
	}
	
	public Integer totalNumberOf(String position) 
	{
		if(position.equals("GoalKeeper"))
			return goalKeeper[0];
		
		if(position.equals("Defense"))
			return defenders[0];
		
		if(position.equals("MidFielder"))
			return midFielders[0];
		
		if(position.equals("Striker"))
			return strikers[0];

		return null;
	}
	
	public Integer partialNumberOf(String position) 
	{
		if(position.equals("GoalKeeper"))
			return goalKeeper[1];
		
		if(position.equals("Defense"))
			return defenders[1];
		
		if(position.equals("MidFielder"))
			return midFielders[1];
		
		if(position.equals("Striker"))
			return strikers[1];

		return null;
	}
	
	@Override
	public String toString()
	{
		return defenders[0]+"-"+midFielders[0]+"-"+strikers[0];
	}
	
	public String getAlignament() 
	{
		return defenders[0]+"-"+midFielders[0]+"-"+strikers[0];
	}

	public int numOfPlayers() 
	{	
		return goalKeeper[0]+defenders[0]+midFielders[0]+strikers[0];
	}
}
